/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.curso.delegate.service.impl;

import com.curso.delegate.feign.FeignDoctor;
import com.curso.delegate.model.dto.DoctorDto;
import com.curso.delegate.service.IDoctorService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DefaultDoctorService implements IDoctorService{

    @Autowired
    FeignDoctor feignDoctor;

    @Override
    public DoctorDto guardarDoctor(DoctorDto doctorDto) {
        return feignDoctor.guardarDoctor(doctorDto);
    }

    @Override
    public List<DoctorDto> listDoctors(){
      return feignDoctor.listarDoctores();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.curso.delegate.service.impl;

import com.curso.delegate.feign.FeignUser;
import com.curso.delegate.model.dto.RequestIdUserDto;
import com.curso.delegate.model.dto.UserDto;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.curso.delegate.service.IUserService;

@Service
public class DefaultUserService implements IUserService{

    @Autowired
    FeignUser feignUser;

    @Override
    public UserDto guardarUsuario(UserDto userDto) {
        return feignUser.guardarUsuario(userDto);
    }

    @Override
    public List<UserDto> listUsers(RequestIdUserDto requestIdUserDto){
        return feignUser.listUserById(requestIdUserDto);
    }
}

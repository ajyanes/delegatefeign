/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.curso.delegate.service.impl;

import com.curso.delegate.model.dto.CloudDto;
import com.curso.delegate.model.dto.DoctorDto;
import com.curso.delegate.model.dto.DoctorInfoDto;
import com.curso.delegate.model.dto.RequestIdUserDto;
import com.curso.delegate.model.dto.ResponseCloudDto;
import com.curso.delegate.model.dto.UserDto;
import com.curso.delegate.service.ICLoudService;
import com.curso.delegate.service.IDelegateService;
import com.curso.delegate.service.IDoctorService;
import com.curso.delegate.service.IUserService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class DefaultDelegateService implements IDelegateService {

  @Autowired
  IUserService iUserService;

  @Autowired
  IDoctorService iDoctorService;
  
  @Autowired
  ICLoudService iCloudService;

  @Override
  public DoctorDto guardarInfo(DoctorInfoDto doctorInfoDto) {
    DoctorDto ddto = null;
    CloudDto cloudDto = new CloudDto();
    ResponseCloudDto responseCloudDto = null;
    cloudDto.setDoctorNumber(doctorInfoDto.getIdmedico());
    responseCloudDto = iCloudService.validate(cloudDto);

    if(!"Success".equals(responseCloudDto.getMessage())){
      return ddto;
    }else {
      UserDto udto = iUserService.guardarUsuario(buildUserDto(doctorInfoDto));
      ddto = iDoctorService.guardarDoctor(buildDoctorDto(doctorInfoDto, udto.getId()));
    }

    return ddto;
  }

  @Override
  public List<DoctorInfoDto> listarDoctores() {

    List<DoctorDto> listaDoctores = iDoctorService.listDoctors();

    RequestIdUserDto requestUser = getListUsers(listaDoctores);

    List<UserDto> listaUsers = iUserService.listUsers(requestUser);

    return buildDoctorInfoDto(listaDoctores, listaUsers);
  }

  public UserDto buildUserDto(DoctorInfoDto doctorInfoDto) {
    UserDto userDto = new UserDto();
    userDto.setName(doctorInfoDto.getName());
    userDto.setLastname(doctorInfoDto.getLastname());
    userDto.setEmail(doctorInfoDto.getEmail());
    userDto.setClave(doctorInfoDto.getClave());

    return userDto;
  }

  public DoctorDto buildDoctorDto(DoctorInfoDto doctorInfoDto, Long id) {
    DoctorDto doctorDto = new DoctorDto();
    doctorDto.setIdUsuario(id);
    doctorDto.setIdMedico(doctorInfoDto.getIdmedico());
    doctorDto.setSpeciality(doctorInfoDto.getSpeciality());
    doctorDto.setUniversity(doctorInfoDto.getUniversity());

    return doctorDto;
  }

  public RequestIdUserDto getListUsers(List<DoctorDto> listaDoctores) {
    List<Long> listIdUser = new ArrayList();
    RequestIdUserDto requestIdUserDto = new RequestIdUserDto();
    for (DoctorDto ddto : listaDoctores) {
      listIdUser.add(ddto.getIdUsuario());
    }

    requestIdUserDto.setListFind(listIdUser);

    return requestIdUserDto;
  }

  public List<DoctorInfoDto> buildDoctorInfoDto(List<DoctorDto> listaDoctores,
      List<UserDto> listaUsers) {
    List<DoctorInfoDto> list = new ArrayList();
    for (DoctorDto ddto : listaDoctores) {
      DoctorInfoDto diDto = new DoctorInfoDto();
      diDto.setSpeciality(ddto.getSpeciality());
      diDto.setUniversity(ddto.getUniversity());
      diDto.setIdmedico(ddto.getIdMedico());
      for (UserDto udto : listaUsers) {
        if (ddto.getIdUsuario() == udto.getId()) {
          diDto.setName(udto.getName());
          diDto.setLastname(udto.getLastname());
          diDto.setClave(udto.getClave());
          diDto.setEmail(udto.getEmail());
        }
      }
      ddto.getIdUsuario();

      list.add(diDto);
    }

    return list;
  }


}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.curso.delegate.service.impl;

import com.curso.delegate.feign.FeignCloud;
import com.curso.delegate.model.dto.CloudDto;
import com.curso.delegate.model.dto.ResponseCloudDto;
import com.curso.delegate.service.ICLoudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ayanes
 */
@Service
public class DefaultCloudService implements ICLoudService{

    @Autowired
    FeignCloud feignCloud;
    
    @Override
    public ResponseCloudDto validate(CloudDto cloudDto) {
        return feignCloud.validateDoctor(cloudDto);
    }
    
}

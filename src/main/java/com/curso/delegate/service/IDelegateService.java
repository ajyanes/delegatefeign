/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.curso.delegate.service;

import com.curso.delegate.model.dto.DoctorDto;
import com.curso.delegate.model.dto.DoctorInfoDto;
import java.util.List;


public interface IDelegateService {

    DoctorDto guardarInfo(DoctorInfoDto doctorInfoDto);
    List<DoctorInfoDto> listarDoctores();
}

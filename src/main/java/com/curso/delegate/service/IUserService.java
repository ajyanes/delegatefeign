/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.curso.delegate.service;

import com.curso.delegate.model.dto.RequestIdUserDto;
import com.curso.delegate.model.dto.UserDto;
import java.util.List;

/**
 *
 * @author ayanes
 */
public interface IUserService {

    UserDto guardarUsuario(UserDto userDto);
    List<UserDto> listUsers(RequestIdUserDto requestIdUserDto);
}

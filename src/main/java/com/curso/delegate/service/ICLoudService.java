package com.curso.delegate.service;

import com.curso.delegate.model.dto.CloudDto;
import com.curso.delegate.model.dto.ResponseCloudDto;

public interface ICLoudService {

  ResponseCloudDto validate(CloudDto cloudDto);
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.curso.delegate.feign;

import com.curso.delegate.model.dto.DoctorDto;
import java.util.List;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "doctor-service")
public interface FeignDoctor {

    @PostMapping("/doctor/guardarDoctor")
    DoctorDto guardarDoctor(DoctorDto doctorDto);

    @GetMapping("/doctor/listarDoctores")
    List<DoctorDto> listarDoctores();
}

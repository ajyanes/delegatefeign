package com.curso.delegate.feign;

import com.curso.delegate.model.dto.CloudDto;
import com.curso.delegate.model.dto.ResponseCloudDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "cloud-service")
public interface FeignCloud {

  @PostMapping("/validate/doctor")
  public ResponseCloudDto validateDoctor(CloudDto cloudDto);

}

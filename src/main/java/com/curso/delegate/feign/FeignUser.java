/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.curso.delegate.feign;

import com.curso.delegate.model.dto.RequestIdUserDto;
import com.curso.delegate.model.dto.UserDto;
import java.util.List;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "user-service")
public interface FeignUser {

    @PostMapping("/user/guardarUser")
    public UserDto guardarUsuario(UserDto userDto);

    @GetMapping("/user/listUserById")
    public List<UserDto> listUserById(RequestIdUserDto requestIdUserDto);
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.curso.delegate.controller;

import com.curso.delegate.model.dto.DoctorDto;
import com.curso.delegate.model.dto.DoctorInfoDto;
import com.curso.delegate.service.IDelegateService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/delegate")
public class DelegateController {

    @Autowired
    IDelegateService delegateService;

    @PostMapping("/guardarDoctor")
    public DoctorDto guardarDoctor(@RequestBody final DoctorInfoDto doctorInfoDto ){

        return delegateService.guardarInfo(doctorInfoDto);
    }

    @GetMapping("/listDoctores")
    public List<DoctorInfoDto> listarDoctores(){

      return delegateService.listarDoctores();
    }
}
